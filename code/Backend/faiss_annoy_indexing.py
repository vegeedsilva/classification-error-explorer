import pyarrow.parquet as pq
from dataclasses import dataclass
import faiss
import numpy as np
import random
from pathlib import Path
import sys
import resource
import os
from collections import defaultdict
import gc
import json
import faiss
from annoy import AnnoyIndex
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import torch
# import tensorflow as tf


def memory_limit():
    soft, hard = resource.getrlimit(resource.RLIMIT_AS)
    resource.setrlimit(resource.RLIMIT_AS, (int (get_memory() * 1024 * 0.8), hard))

def get_memory():
    with open('/proc/meminfo', 'r') as mem:
        free_memory = 0
        for i in mem:
            sline = i.split()
            if str(sline[0]) in ('MemFree:', 'Buffers:', 'Cached:'):
                free_memory += int(sline[1])
    print("Free memory: ", free_memory)
    return free_memory

def store_index_mappings(embpath):

    id_index = defaultdict(list)
    start_id = 0

    # print(sorted(os.listdir(embpath)))

    key = 0

    for parquet_file in sorted(os.listdir(embpath)):
        if parquet_file.endswith('.parquet.gzip'):
            path = embpath +'/' + parquet_file
            emb = pq.read_table(path).to_pandas()
            end_id = start_id + len(emb) - 1
            # foldername is the key
            foldername = str(parquet_file)
            foldername = foldername[foldername.index('n') : foldername.index('.parquet.gzip')]
            id_index[foldername].append([start_id, end_id])
            print(id_index)
            if(key% 100 == 0):
                print("folder: ",  key)
            gc.collect()
            key = key + 1
            start_id = end_id + 1



    with open('/project/dsilva/Implementation/ClassificationErrorExplorer/imagenet_index_mappings.json', 'w') as fp:
        json.dump(id_index,fp)

    return

# def build_index(emb):
#     # d = emb.shape[1]
#     # xb = emb
#     # xb = xb /np.linalg.norm(xb)
#     # index = faiss.IndexFlatIP(d)
#     # index.add(xb)
#
#     # mat = faiss.PCAMatrix(51200, 1000)
#     # mat.train(emb)
#     # assert mat.is_trained
#     # tr = mat.apply_py(emb)
#     # print (tr ** 2).sum(0)
#     # emb = emb/np.linalg.norm(emb)
#     #
#     # index.train(emb)
#     # index.add(emb)
#     return

def get_folderlist( ):
    json_path = '/data/imagenet/imagenet_class_index.json'
    with open(json_path) as f:
        data = json.load(f)
    foldername = [ ]
    for row in data:
        foldername.append(data[row][0])
        # print(foldername)
    print(len(foldername))
    return foldername


def get_foldername(classID):
    json_path = '/data/imagenet/imagenet_class_index.json'
    with open(json_path) as f:
        data = json.load(f)
    foldername = data[classID][0]
    print(data[classID][1])
    return foldername


def write_index(embpath):
    index_path = '/project/dsilva/Implementation/ClassificationErrorExplorer/faiss_index/'

    dims = 51200
    index = faiss.IndexFlatL2(dims)
    # index = faiss.index_factory( 51200,"IVF65536_HNSW32,PQ64" )
    folderlist = get_folderlist()

    # for parquet_file in sorted(os.listdir(embpath)):
    #     if parquet_file.endswith('.parquet.gzip'):

    # for i in range(len(folderlist)):
    for i in range(874, 1000):
        parquet_file = Path(embpath + str(folderlist[i]) + '.parquet.gzip')
        if parquet_file.is_file():
            emb_df = pq.read_table(parquet_file).to_pandas()
            emb = np.stack(emb_df["embedding"].to_numpy())
            emb = emb/np.linalg.norm(emb)
            index.add(emb)
            faiss.write_index(index , index_path + str(folderlist[i]) +'.index' )
            gc.collect()
            if(i%100 == 0):
                print("Index saved for ", i)

    return


def fill_missing(emb_path):
    index_path = '/project/dsilva/Implementation/ClassificationErrorExplorer/faiss_index/'
    dims = 51200
    index = faiss.IndexFlatIP(dims)
    folderlist = get_folderlist()

    for i in range(len(folderlist)):
        index_file = Path(index_path + str(folderlist[i]) + '.index')
        if not (index_file.is_file()):
            parquet_file = Path(emb_path + str(folderlist[i]) + '.parquet.gzip')
            if parquet_file.is_file():
                emb_df = pq.read_table(parquet_file).to_pandas()
                emb = np.stack(emb_df["embedding"].to_numpy())
                emb = emb/np.linalg.norm(emb)
                index.add(emb)
                faiss.write_index(index , index_path + str(folderlist[i]) +'.index' )
                gc.collect()
                print(" Index saved ", i)


    return
def get_imagename(emb_df , indexlist):
    imagelist =[]
    print("Index list",  indexlist)
    # print(" dataframe, ", len(emb_df))
    for i in indexlist:
        name = emb_df.iloc[i]["imagename"]
        imagelist.append(name)

    return imagelist
def faiss_search(index, test_emb, train_emb_df):
    k = 5
    D, I = index.search(test_emb, k)
    print(D)
    similar_images = get_imagename(train_emb_df, I[0])
    return D, similar_images

def build_index(foldername):
    # gpu_ids = "0"
    # os.environ['CUDA_VISIBLE_DEVICES'] = gpu_ids

    imagenet_emb_path ='/project/dsilva/Implementation/ClassificationErrorExplorer/imagenet_embeddings/'
    dims = 51200
    index = faiss.IndexFlatIP(dims)
    parquet_file = imagenet_emb_path + foldername +'.parquet.gzip'
    emb_df = pq.read_table(parquet_file).to_pandas()
    emb = np.stack(emb_df["embedding"].to_numpy())
    emb = emb/np.linalg.norm(emb)
    # gpu_index = faiss.index_cpu_to_all_gpus(index)
    # gpu_index.add(emb)
    index.add(emb)
    # return gpu_index, emb_df
    return index, emb_df


def search (classID, imagename, test_image_folder ):

    index_path = '/project/dsilva/Implementation/ClassificationErrorExplorer/faiss_index/'
    test_embpath = '/project/dsilva/Implementation/ClassificationErrorExplorer/objectnet_embeddings/'
    similar_images = defaultdict(list)
    foldername = str(get_foldername(str(classID)))
    # print("Folder found", foldername)
    # index = faiss.read_index(index_path + foldername + '.index')
    index, train_emb_df = build_index(foldername)
    # print("Index built")
    test_emb_df = pq.read_table(test_embpath+test_image_folder +'.parquet.gzip').to_pandas()
    # print(type(test_emb_df))
    test_emb = test_emb_df.loc[test_emb_df['imagename'] == imagename]
    test_emb_np = np.stack(test_emb["embedding"].to_numpy())
    test_emb_np = test_emb_np/np.linalg.norm(test_emb_np)
    scores, sim_imagelist = faiss_search(index,test_emb_np, train_emb_df)
    print("Similar images", sim_imagelist)
    test_image_path = '/data/objectnet/objectnet-1.0/images/' +  test_image_folder + '/'+ imagename
    display_image(test_image_path)
    for image in sim_imagelist:
        train_image_path = '/data/imagenet/ILSVRC2012_img_train/' + foldername + '/' + image
        display_image(train_image_path)

    return scores, sim_imagelist

def annoy_search(index, test_emb, train_emb_df ):
    k = 5
    I, D = index.get_nns_by_vector(test_emb[0], k ,include_distances=True)
    print("Nearest neighbours", I)
    similar_images = get_imagename(train_emb_df, I)
    return D, similar_images

def build_annoy_index(foldername):
    index_path = '/project/dsilva/Implementation/ClassificationErrorExplorer/annoy_index/'
    dims = 51200
    index = AnnoyIndex(dims, 'euclidean')
    imagenet_emb_path ='/project/dsilva/Implementation/ClassificationErrorExplorer/imagenet_embeddings/'
    parquet_file = imagenet_emb_path + foldername +'.parquet.gzip'
    emb_df = pq.read_table(parquet_file).to_pandas()
    emb = np.stack(emb_df["embedding"].to_numpy())
    # emb = emb/np.linalg.norm(emb)

    for i in range(len(emb)):
        index.add_item(i, emb[i])
    index.build(100)
    index.save(index_path + foldername + '.ann')
    return index,emb_df

def write_annoy_index(classID, imagename, test_image_folder):
    index_path = '/project/dsilva/Implementation/ClassificationErrorExplorer/annoy_index/'
    test_embpath = '/project/dsilva/Implementation/ClassificationErrorExplorer/objectnet_embeddings/'
    foldername = str(get_foldername(str(classID)))


    index, train_emb_df = build_annoy_index(foldername)
    test_emb_df = pq.read_table(test_embpath+test_image_folder +'.parquet.gzip').to_pandas()
    test_emb = test_emb_df.loc[test_emb_df['imagename'] == imagename]
    test_emb_np = np.stack(test_emb["embedding"].to_numpy())
    test_emb_np = test_emb_np/np.linalg.norm(test_emb_np)
    index.load(index_path + foldername + '.ann')
    scores, sim_imagelist = annoy_search(index,test_emb_np, train_emb_df)
    print("Similar images", sim_imagelist)
    test_image_path = '/data/objectnet/objectnet-1.0/images/' +  test_image_folder + '/'+ imagename
    display_image(test_image_path)
    for image in sim_imagelist:
        train_image_path = '/data/imagenet/ILSVRC2012_img_train/' + foldername + '/' + image
        display_image(train_image_path)
    return scores, sim_imagelist

def display_image(path):
    image = mpimg.imread(path)
    plt.imshow(image)
    plt.show()
    return



def build_index_gpu():

    dims = 51200
    res = faiss.StandardGpuResources()
    # quantizer = faiss.IndexFlatL2(dims)
    index =  faiss.IndexFlatL2(dims)
    ncentroids =1000
    code_size = 16
    # index = faiss.IndexIVFPQ(quantizer, dims, ncentroids, code_size, 8 )
    imagenet_emb_path = '/project/dsilva/Implementation/ClassificationErrorExplorer/imagenet_embeddings/'
    i = 0
    for parquet_file in sorted(os.listdir(imagenet_emb_path)):
        if parquet_file.endswith('.parquet.gzip'):
            emb = pq.read_table(imagenet_emb_path + parquet_file).to_pandas()
            emb_np = np.stack(emb["embedding"].to_numpy())
            # emb_np = emb_np / np.linalg.norm(emb_np)
            # print(emb_np[0].shape)
            index.add(emb_np)
            if(i % 100 == 0):
                print("Folder", i)
            i = i + 1





    return index
def faiss_gpu_search(imagename, test_image_folder):
    index_path = '/project/dsilva/Implementation/ClassificationErrorExplorer/faiss_index/'
    test_embpath = '/project/dsilva/Implementation/ClassificationErrorExplorer/objectnet_embeddings/'
    test_emb_df = pq.read_table(test_embpath+test_image_folder +'.parquet.gzip').to_pandas()
    test_emb = test_emb_df.loc[test_emb_df['imagename'] == imagename]
    test_emb_np = np.stack(test_emb["embedding"].to_numpy())
    # test_emb_np = test_emb_np/np.linalg.norm(test_emb_np)
    index = build_index_gpu()
    faiss.write_index(index , index_path + 'faiss.index' )

    return
def main():
    imagenet_emb_path = '/project/dsilva/Implementation/ClassificationErrorExplorer/imagenet_embeddings/'

    device = torch.cuda.current_device()
    print(torch.cuda.get_device_name(device))
    # store_index_mappings(imagenet_emb_path)
    test_image = ' '
    predicted_class = ' '
    actual_class = 'pitcher'
    # write_index(imagenet_emb_path)
    # -------------------------------------------
    # Test #1
    # actual_classID = 725
    # predclassId = "463"
    # image_name = "1c85018ba1d043c.png"
    # actual_class = 'pitcher'
    # ----------------------
    # Test # 2
    # actual_classID = 679
    # predclassId = "644"
    # image_name = "ad8f1b497747491.png"
    # actual_class = 'necklace'

    # ------------------------------------------------
    # actual_classID = 790
    # predclassId = "588"
    # image_name = "60447f26e06041e.png"
    # actual_class = 'basket'
    # -----------------------------------------
    # Test Case #3
    # actual_classID = 545
    # predclassId = 837
    # image_name = "ea8c2c8647ca49e.png"
    # actual_class = 'tanktop'
    # ---------------------------------------------------------------------
    actual_classID = 0
    predclassId = "664"
    image_name = "6cbea218c0f149a.png"
    actual_class = 'monitor'
    # FAISS Search
    search(predclassId,  image_name, actual_class)
    search(actual_classID, image_name,  actual_class)
    # -------------------------------------------------------
    # print (sys.prefix)  #Prints the environment
    # tf.config.experimental.list_physical_devices('GPU')
    # with tf.device('/GPU:0'):
    # ------------------------------------------------------------------------
    # ANNOY search
    write_annoy_index(predclassId,  image_name, actual_class)
    # write_annoy_index(actual_classID,  image_name, actual_class)

    return

if __name__ == '__main__':
    memory_limit() # Limitates maximun memory usage
    try:
        main()
    except MemoryError:
        sys.stderr.write('\n\nERROR: Memory Exception\n')
        sys.exit(1)

# Free memory:  258327992
# RuntimeError: The NVIDIA driver on your system is too old (found version 10010). Please update your GPU driver by downloading and installing a new version from the URL: http://www.nvidia.com/Download/index.aspx Alternatively, go to: https://pytorch.org to install a PyTorch version that has been compiled with your version of the CUDA driver.


# Faiss assertion 'err__ == cudaSuccess' failed in int faiss::gpu::getNumDevices() at /root/miniconda3/conda-bld/faiss-pkg_1613957773487/work/faiss/gpu/utils/DeviceUtils.cu:36; details: CUDA error 35 CUDA driver version is insufficient for CUDA runtime version
# https://github.com/facebookresearch/faiss/issues/1295
# https://stackoverflow.com/questions/60272028/how-does-anaconda-pick-cudatoolkit